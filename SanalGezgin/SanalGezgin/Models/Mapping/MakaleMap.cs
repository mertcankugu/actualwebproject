using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class MakaleMap : EntityTypeConfiguration<Makale>
    {
        public MakaleMap()
        {
            // Primary Key
            this.HasKey(t => t.makaleId);

            // Properties
            this.Property(t => t.makaleBaslik)
                .IsRequired()
                .HasMaxLength(150);

            this.Property(t => t.makaleIcerik)
                .IsRequired();

            this.Property(t => t.videoYolu)
                .HasMaxLength(500);

            this.Property(t => t.resim1)
                .HasMaxLength(500);

            this.Property(t => t.resim2)
                .HasMaxLength(500);

            this.Property(t => t.resim3)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Makale");
            this.Property(t => t.makaleId).HasColumnName("makaleId");
            this.Property(t => t.makaleBaslik).HasColumnName("makaleBaslik");
            this.Property(t => t.makaleIcerik).HasColumnName("makaleIcerik");
            this.Property(t => t.makaleTarih).HasColumnName("makaleTarih");
            this.Property(t => t.kategoriId).HasColumnName("kategoriId");
            this.Property(t => t.editorId).HasColumnName("editorId");
            this.Property(t => t.videoYolu).HasColumnName("videoYolu");
            this.Property(t => t.resim1).HasColumnName("resim1");
            this.Property(t => t.resim2).HasColumnName("resim2");
            this.Property(t => t.resim3).HasColumnName("resim3");
            this.Property(t => t.makaleAktif).HasColumnName("makaleAktif");
            this.Property(t => t.populer).HasColumnName("populer");
            this.Property(t => t.sonHaber).HasColumnName("sonHaber");

            // Relationships
            this.HasOptional(t => t.Editor)
                .WithMany(t => t.Makales)
                .HasForeignKey(d => d.editorId);
            this.HasOptional(t => t.Kategori)
                .WithMany(t => t.Makales)
                .HasForeignKey(d => d.kategoriId);

        }
    }
}
