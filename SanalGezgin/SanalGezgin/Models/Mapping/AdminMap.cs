using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class AdminMap : EntityTypeConfiguration<Admin>
    {
        public AdminMap()
        {
            // Primary Key
            this.HasKey(t => t.adminId);

            // Properties
            this.Property(t => t.adminAdiSoyadi)
                .HasMaxLength(50);

            this.Property(t => t.adminSifre)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Admin");
            this.Property(t => t.adminId).HasColumnName("adminId");
            this.Property(t => t.adminAdiSoyadi).HasColumnName("adminAdiSoyadi");
            this.Property(t => t.adminSifre).HasColumnName("adminSifre");
        }
    }
}
