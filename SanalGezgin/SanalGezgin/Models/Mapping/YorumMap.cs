using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class YorumMap : EntityTypeConfiguration<Yorum>
    {
        public YorumMap()
        {
            // Primary Key
            this.HasKey(t => t.yorumId);

            // Properties
            this.Property(t => t.yorumAdiSoyadi)
                .HasMaxLength(100);

            this.Property(t => t.yorumMail)
                .HasMaxLength(100);

            this.Property(t => t.yorumIcerik)
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Yorum");
            this.Property(t => t.yorumId).HasColumnName("yorumId");
            this.Property(t => t.yorumAdiSoyadi).HasColumnName("yorumAdiSoyadi");
            this.Property(t => t.yorumMail).HasColumnName("yorumMail");
            this.Property(t => t.yorumIcerik).HasColumnName("yorumIcerik");
            this.Property(t => t.makaleId).HasColumnName("makaleId");
            this.Property(t => t.eklenmeTatihi).HasColumnName("eklenmeTatihi");
            this.Property(t => t.yorumOnay).HasColumnName("yorumOnay");

            // Relationships
            this.HasOptional(t => t.Makale)
                .WithMany(t => t.Yorums)
                .HasForeignKey(d => d.makaleId);

        }
    }
}
