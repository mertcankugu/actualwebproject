using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class EditorMap : EntityTypeConfiguration<Editor>
    {
        public EditorMap()
        {
            // Primary Key
            this.HasKey(t => t.editorId);

            // Properties
            this.Property(t => t.editorAdiSoyadi)
                .HasMaxLength(50);

            this.Property(t => t.editorKadi)
                .HasMaxLength(50);

            this.Property(t => t.editorSifre)
                .HasMaxLength(50);

            this.Property(t => t.editorMail)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Editor");
            this.Property(t => t.editorId).HasColumnName("editorId");
            this.Property(t => t.editorAdiSoyadi).HasColumnName("editorAdiSoyadi");
            this.Property(t => t.editorKadi).HasColumnName("editorKadi");
            this.Property(t => t.editorSifre).HasColumnName("editorSifre");
            this.Property(t => t.editorTarih).HasColumnName("editorTarih");
            this.Property(t => t.editorMail).HasColumnName("editorMail");
            this.Property(t => t.editorAktif).HasColumnName("editorAktif");
        }
    }
}
