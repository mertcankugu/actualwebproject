using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class MakaleResimlerMap : EntityTypeConfiguration<MakaleResimler>
    {
        public MakaleResimlerMap()
        {
            // Primary Key
            this.HasKey(t => new { t.makaleId, t.resimId });

            // Properties
            this.Property(t => t.makaleId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.resimId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            this.ToTable("MakaleResimler");
            this.Property(t => t.makaleId).HasColumnName("makaleId");
            this.Property(t => t.resimId).HasColumnName("resimId");

            // Relationships
            

        }
    }
}
