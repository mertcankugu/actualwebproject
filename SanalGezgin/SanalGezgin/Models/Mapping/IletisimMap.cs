using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class IletisimMap : EntityTypeConfiguration<Iletisim>
    {
        public IletisimMap()
        {
            // Primary Key
            this.HasKey(t => t.iletisimId);

            // Properties
            this.Property(t => t.iletisimAdiSoyadi)
                .HasMaxLength(50);

            this.Property(t => t.iletisimMail)
                .HasMaxLength(50);

            this.Property(t => t.iletisimKonu)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Iletisim");
            this.Property(t => t.iletisimId).HasColumnName("iletisimId");
            this.Property(t => t.iletisimAdiSoyadi).HasColumnName("iletisimAdiSoyadi");
            this.Property(t => t.iletisimMail).HasColumnName("iletisimMail");
            this.Property(t => t.iletisimKonu).HasColumnName("iletisimKonu");
            this.Property(t => t.iletisimIcerik).HasColumnName("iletisimIcerik");
        }
    }
}
