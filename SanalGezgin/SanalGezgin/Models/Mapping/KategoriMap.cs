using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class KategoriMap : EntityTypeConfiguration<Kategori>
    {
        public KategoriMap()
        {
            // Primary Key
            this.HasKey(t => t.kategoriId);

            // Properties
            this.Property(t => t.kategoriAdi)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("Kategori");
            this.Property(t => t.kategoriId).HasColumnName("kategoriId");
            this.Property(t => t.kategoriAdi).HasColumnName("kategoriAdi");
        }
    }
}
