using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class SosyalAglarMap : EntityTypeConfiguration<SosyalAglar>
    {
        public SosyalAglarMap()
        {
            // Primary Key
            this.HasKey(t => t.sosyalId);

            // Properties
            this.Property(t => t.sosyalAdi)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("SosyalAglar");
            this.Property(t => t.sosyalId).HasColumnName("sosyalId");
            this.Property(t => t.sosyalAdi).HasColumnName("sosyalAdi");
            this.Property(t => t.sosyalHit).HasColumnName("sosyalHit");
        }
    }
}
