using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class ResimMap : EntityTypeConfiguration<Resim>
    {
        public ResimMap()
        {
            // Primary Key
            this.HasKey(t => t.resimId);

            // Properties
            this.Property(t => t.resimId)
                .IsRequired()
                .HasMaxLength(500);

            this.Property(t => t.resimAdi)
                .HasMaxLength(150);

            this.Property(t => t.resimYolu)
                .HasMaxLength(250);

            this.Property(t => t.videoYolu)
                .HasMaxLength(500);

            // Table & Column Mappings
            this.ToTable("Resim");
            this.Property(t => t.resimId).HasColumnName("resimId");
            this.Property(t => t.resimAdi).HasColumnName("resimAdi");
            this.Property(t => t.resimYolu).HasColumnName("resimYolu");
            this.Property(t => t.videoYolu).HasColumnName("videoYolu");
            this.Property(t => t.eklenmeTarihi).HasColumnName("eklenmeTarihi");
        }
    }
}
