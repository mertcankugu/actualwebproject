using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace SanalGezgin.Models.Mapping
{
    public class BannerMap : EntityTypeConfiguration<Banner>
    {
        public BannerMap()
        {
            // Primary Key
            this.HasKey(t => t.bannerId);

            // Properties
            this.Property(t => t.bannerBaslik)
                .HasMaxLength(150);

            this.Property(t => t.bannerIcerik)
                .HasMaxLength(250);

            this.Property(t => t.bannerYolu)
                .HasMaxLength(250);

            this.Property(t => t.bannerResim)
                .HasMaxLength(250);

            // Table & Column Mappings
            this.ToTable("Banner");
            this.Property(t => t.bannerId).HasColumnName("bannerId");
            this.Property(t => t.bannerBaslik).HasColumnName("bannerBaslik");
            this.Property(t => t.bannerIcerik).HasColumnName("bannerIcerik");
            this.Property(t => t.bannerYolu).HasColumnName("bannerYolu");
            this.Property(t => t.bannerResim).HasColumnName("bannerResim");
        }
    }
}
