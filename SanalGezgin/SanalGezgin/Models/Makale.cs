using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class Makale
    {
        public Makale()
        {
            this.Yorums = new List<Yorum>();
        }

        public int makaleId { get; set; }
        public string makaleBaslik { get; set; }
        public string makaleIcerik { get; set; }
        public Nullable<System.DateTime> makaleTarih { get; set; }
        public Nullable<int> kategoriId { get; set; }
        public Nullable<int> editorId { get; set; }
        public string videoYolu { get; set; }
        public string resim1 { get; set; }
        public string resim2 { get; set; }
        public string resim3 { get; set; }
        public bool makaleAktif { get; set; }
        public Nullable<bool> populer { get; set; }
        public Nullable<bool> sonHaber { get; set; }
        public virtual Editor Editor { get; set; }
        public virtual Kategori Kategori { get; set; }
        public virtual ICollection<Yorum> Yorums { get; set; }
    }
}
