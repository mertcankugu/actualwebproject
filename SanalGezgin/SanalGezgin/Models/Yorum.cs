using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class Yorum
    {
        public int yorumId { get; set; }
        public string yorumAdiSoyadi { get; set; }
        public string yorumMail { get; set; }
        public string yorumIcerik { get; set; }
        public Nullable<int> makaleId { get; set; }
        public Nullable<System.DateTime> eklenmeTatihi { get; set; }
        public Nullable<bool> yorumOnay { get; set; }
        public virtual Makale Makale { get; set; }
    }
}
