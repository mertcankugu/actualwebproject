using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class Banner
    {
        public int bannerId { get; set; }
        public string bannerBaslik { get; set; }
        public string bannerIcerik { get; set; }
        public string bannerYolu { get; set; }
        public string bannerResim { get; set; }
    }
}
