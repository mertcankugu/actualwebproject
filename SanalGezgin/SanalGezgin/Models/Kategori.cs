using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class Kategori
    {
        public Kategori()
        {
            this.Makales = new List<Makale>();
        }

        public int kategoriId { get; set; }
        public string kategoriAdi { get; set; }
        public virtual ICollection<Makale> Makales { get; set; }
    }
}
