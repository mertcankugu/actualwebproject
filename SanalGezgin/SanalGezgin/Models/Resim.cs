using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class Resim
    {
        public string resimId { get; set; }
        public string resimAdi { get; set; }
        public string resimYolu { get; set; }
        public string videoYolu { get; set; }
        public Nullable<System.DateTime> eklenmeTarihi { get; set; }
    }
}
