using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class SosyalAglar
    {
        public int sosyalId { get; set; }
        public string sosyalAdi { get; set; }
        public Nullable<int> sosyalHit { get; set; }
    }
}
