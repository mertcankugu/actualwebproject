using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class Admin
    {
        public int adminId { get; set; }
        public string adminAdiSoyadi { get; set; }
        public string adminSifre { get; set; }
    }
}
