using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class Etiket
    {
        public Etiket()
        {
            this.Makales = new List<Makale>();
        }

        public int etiketId { get; set; }
        public string etiketAdi { get; set; }
        public virtual ICollection<Makale> Makales { get; set; }
    }
}
