using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class Editor
    {
        public Editor()
        {
            this.Makales = new List<Makale>();
        }

        public int editorId { get; set; }
        public string editorAdiSoyadi { get; set; }
        public string editorKadi { get; set; }
        public string editorSifre { get; set; }
        public Nullable<System.DateTime> editorTarih { get; set; }
        public string editorMail { get; set; }
        public bool editorAktif { get; set; }
        public virtual ICollection<Makale> Makales { get; set; }
    }
}
