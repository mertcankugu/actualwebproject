using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using SanalGezgin.Models.Mapping;

namespace SanalGezgin.Models
{
    public partial class SanalContext : DbContext
    {
        static SanalContext()
        {
            Database.SetInitializer<SanalContext>(null);
        }

        public SanalContext()
            : base("Name=SanalGezginVerileriContext")
        {
        }

        public DbSet<Admin> Admins { get; set; }
        public DbSet<Banner> Banners { get; set; }
        public DbSet<Editor> Editors { get; set; }
        public DbSet<Iletisim> Iletisims { get; set; }
        public DbSet<Kategori> Kategoris { get; set; }
        public DbSet<Makale> Makales { get; set; }
        public DbSet<SosyalAglar> SosyalAglars { get; set; }
        public DbSet<Yorum> Yorums { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AdminMap());
            modelBuilder.Configurations.Add(new BannerMap());
            modelBuilder.Configurations.Add(new EditorMap());
            modelBuilder.Configurations.Add(new IletisimMap());
            modelBuilder.Configurations.Add(new KategoriMap());
            modelBuilder.Configurations.Add(new MakaleMap());
            modelBuilder.Configurations.Add(new SosyalAglarMap());
            modelBuilder.Configurations.Add(new YorumMap());
        }
    }
}
