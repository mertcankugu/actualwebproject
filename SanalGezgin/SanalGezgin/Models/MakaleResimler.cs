using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class MakaleResimler
    {
        public int makaleId { get; set; }
        public int resimId { get; set; }
        public virtual Makale Makale { get; set; }
    }
}
