using System;
using System.Collections.Generic;

namespace SanalGezgin.Models
{
    public partial class Iletisim
    {
        public int iletisimId { get; set; }
        public string iletisimAdiSoyadi { get; set; }
        public string iletisimMail { get; set; }
        public string iletisimKonu { get; set; }
        public string iletisimIcerik { get; set; }
    }
}
