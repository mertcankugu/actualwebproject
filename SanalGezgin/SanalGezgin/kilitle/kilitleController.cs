﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.kilitle
{
    public class kilitleController : System.Web.Mvc.Controller
    {



        protected override void OnActionExecuted(System.Web.Mvc.ActionExecutedContext filterContext)
        {

            if (Session["editorAktif"] == null || Session["editorAktif"].ToString() != "1")
            {
                filterContext.Result = new RedirectResult("~/SanalPanel");
                return;
            }
            
            base.OnActionExecuted(filterContext);
        }
    }
}