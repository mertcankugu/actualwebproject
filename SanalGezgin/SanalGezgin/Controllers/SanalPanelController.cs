﻿using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.Controllers
{
    public class SanalPanelController : Controller
    {
        //
        // GET: /SanalPanel/

        SanalContext context = new SanalContext();
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult editorGiris(FormCollection editor)
        {

            string editorKadi = Request.Form["editorKadi"];
            string editorSifre = Request.Form["editorSifre"];
            
            if (String.IsNullOrEmpty(editorKadi) && String.IsNullOrEmpty(editorSifre))
            {
                return RedirectToAction("/");
            }
            else if (String.IsNullOrEmpty(editorKadi))
            {
                return RedirectToAction("/");
            }
            else if (string.IsNullOrEmpty(editorSifre))
            {
                return RedirectToAction("/");
            }
            else
            {
                var uye = (from i in context.Editors where i.editorSifre == editorSifre && i.editorKadi == editorKadi select i).SingleOrDefault();



                if (uye == null)
                {
                    return RedirectToAction("/");
                }
                else
                {
                    
                    Session["editorKadi"] = uye.editorAdiSoyadi;
                    Session["editorId"] = uye.editorId;
                    Session["editorAktif"] = "1";
                    
                    return RedirectToAction("../SanalYonetim");
                }




            }
        }
        public ActionResult Cikis()
        {
            Session.Clear();
            return RedirectToAction("/");
        }

        public ActionResult iletisimSayi()
        {   
            return View(context.Iletisims.ToList());
        
        }
        public ActionResult makaleSayi()
        {
            return View(context.Makales.ToList());

        }
        public ActionResult yorumSayi()
        {
            return View(context.Yorums.ToList());

        }
        public ActionResult editorSayi()
        {
            return View(context.Editors.ToList());

        }

    }

}