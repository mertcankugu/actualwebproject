﻿using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace SanalGezgin.Controllers
{
    public class KategoriController : Controller
    {
        //
        // GET: /Kategori/

        SanalContext context = new SanalContext();

        public ActionResult Index(int id)
        {
            return View(id);
        }

        public ActionResult MakaleGetir(int id)
        {
            var data = context.Makales.Where(x => x.kategoriId == id);
            return View("MakaleGetir", data);
        }

    }
}
