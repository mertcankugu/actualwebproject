﻿using SanalGezgin.kilitle;
using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.Controllers
{
    public class editorController : kilitleController
    {
        //
        // GET: /editor/
        SanalContext context = new SanalContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult editor(int id)
        {
           
            return View();
        }




        [HttpPost]
        public ActionResult EditorEkle(FormCollection editor)
        {
            Editor model = new Editor();
            model.editorAdiSoyadi = editor["editorAdiSoyadi"].Trim();
            model.editorKadi = editor["editorKadi"].Trim();
            model.editorMail = editor["editorMail"].Trim();
            model.editorSifre = editor["editorSifre"].Trim();
            model.editorTarih = DateTime.Now;
            model.editorAktif = false;
            
            context.Editors.Add(model);
            context.SaveChanges();
            return RedirectToAction("Index");

        }

        public ActionResult EditorGetir()
        {
            var editor = context.Editors.ToList();
            return View(editor);
        }

        public ActionResult Sil(int id)
        {
            context.Editors.Remove(context.Editors.Find(id));

            context.SaveChanges();
            return View("Index", context.Editors);
        }

    }
}
