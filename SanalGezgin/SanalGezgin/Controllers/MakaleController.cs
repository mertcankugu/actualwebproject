﻿using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Providers.Entities;

namespace SanalGezgin.Controllers
{
    public class MakaleController : Controller
    {
        //
        // GET: /Makale/

        SanalContext context = new SanalContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MakaleDetay(int id)
        {

            

            

            Makale mk = context.Makales.FirstOrDefault(x => x.makaleId == id);
            return View(mk);
        }


        

        [HttpPost]
        public ActionResult YorumYaz(Yorum yorum)
        {
            
            
            yorum.eklenmeTatihi = DateTime.Now;
            yorum.yorumOnay = false;
            context.Yorums.Add(yorum);
            context.SaveChanges();
            return RedirectToAction("MakaleDetay", new { id = yorum.makaleId});
            
        }
        public ActionResult makaleEkle()
        {
            var makale = context.Makales.ToList();
            return View(makale);
        }
        


        

        


    }
}
