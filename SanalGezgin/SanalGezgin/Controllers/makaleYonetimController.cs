﻿using SanalGezgin.kilitle;
using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.Models
{
    public class makaleYonetimController : kilitleController
    {
        //
        // GET: /makaleYonetim/

        SanalContext context = new SanalContext();

        public ActionResult Index()
        {
            var makale = context.Makales.ToList();
            return View(makale);
        }
        
        public ActionResult Sil(int id)
        {
            context.Makales.Remove(context.Makales.Find(id));

            context.SaveChanges();
            return View("Index", context.Makales);
        }
        

    }
}
