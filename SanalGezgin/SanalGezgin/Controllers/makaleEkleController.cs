﻿using SanalGezgin.kilitle;
using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.Controllers
{
    public class makaleEkleController : kilitleController
    {
        //
        // GET: /makaleEkle/


        SanalContext context = new SanalContext();
        public ActionResult Index()
        {
            
            return View();
        }

        public ActionResult makaleEkle()
        {
            var makale = context.Makales.ToList();
            return View(makale);
        }
        public ActionResult kategoriGetir()
        {
            var kategori = context.Kategoris.ToList();
            return View(kategori);
        }

        [HttpPost]
        public ActionResult makaleYaz(Makale makale, HttpPostedFileBase resim1)
        {

            if (resim1 != null)
            {
                string DosyaAdi = Guid.NewGuid().ToString().Replace("-", "");
                string uzanti = System.IO.Path.GetExtension(Request.Files[0].FileName);
                string TamYolYeri = "~/Content/img/makale/" + DosyaAdi + uzanti;
                Request.Files[0].SaveAs(Server.MapPath(TamYolYeri));
                makale.resim1 = DosyaAdi + uzanti;
            }
            makale.makaleTarih = DateTime.Now;
            
            makale.makaleAktif = true;
            makale.populer = true;
            makale.sonHaber = true;
            

            
            context.Makales.Add(makale);
            context.SaveChanges();
            return RedirectToAction("/");

        }
       

        

    }
}
