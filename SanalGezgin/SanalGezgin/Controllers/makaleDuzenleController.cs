﻿using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.Controllers
{
    public class makaleDuzenleController : Controller
    {
        //
        // GET: /makaleDuzenle/
        SanalContext context = new SanalContext();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult makaleDuzenle(int id)
        {
            return View(context.Makales.FirstOrDefault(x => x.makaleId == id));
        }
        [HttpPost]
        public ActionResult makaleDuzenle(Makale makale)
        {
            var mevcut = context.Makales.Find(makale.makaleId);
            mevcut.makaleBaslik = makale.makaleBaslik;
            mevcut.makaleIcerik = makale.makaleIcerik;
            
            context.SaveChanges();
            return RedirectToAction("../makaleYonetim");
        }

    }
}
