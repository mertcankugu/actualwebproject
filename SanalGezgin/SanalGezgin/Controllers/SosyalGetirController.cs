﻿using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.Controllers
{
    public class SosyalGetirController : Controller
    {
        //
        // GET: /SosyalGetir/
        SanalContext context = new SanalContext();

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SosyalGetir()
        {
            var makale = context.Makales.ToList();
            return View(makale);
        }

    }
}
