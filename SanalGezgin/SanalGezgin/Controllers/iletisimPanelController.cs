﻿using SanalGezgin.kilitle;
using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.Controllers
{
    public class iletisimPanelController : kilitleController
    {
        //
        // GET: /iletisimPanel/

        SanalContext context = new SanalContext();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult mesajGetir()
        {
            var iletisim = context.Iletisims.ToList();
            return View(iletisim);
        }
        public ActionResult Sil(int id)
        {
            context.Iletisims.Remove(context.Iletisims.Find(id));
            

            context.SaveChanges();
            return View("Index", context.Iletisims);
        }

    }
}
