﻿using SanalGezgin.kilitle;
using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.Controllers
{
    public class yorumYonetimController : kilitleController
    {
        //
        // GET: /yorumYonetim/
        SanalContext context = new SanalContext();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult YorumGetir()
        {
            var yorum = context.Yorums.ToList();
            return View(yorum);
        }
        public ActionResult Sil(int id)
        {
            context.Yorums.Remove(context.Yorums.Find(id));
            
            context.SaveChanges();
            return View("Index", context.Yorums);
        }

    }
}
