﻿using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace SanalGezgin.Controllers
{
    public class AnasayfaController : Controller
    {
        //
        // GET: /Anasayfa/
        SanalContext context = new SanalContext();

        public ActionResult Index()
        {
            
            
            return View();
        }
        public ActionResult BannerGetir()
        {
            var banner = context.Banners.ToList();//oluşturulan değişken listeye atıldı
            return View(banner); //banner döndürüldü
        }
        public ActionResult KategoriGetir()
        {
            return View(context.Kategoris.ToList());
        }

        public ActionResult MakaleGetir()
        {
            var makale = context.Makales.ToList();
            return View(makale);
        }

        public ActionResult SosyalAglar()
        {
            var sosyal = context.SosyalAglars.ToList();
            return View(sosyal);
        }

        public ActionResult ekipGetir()
        {
            return View(context.Editors.ToList());
        }

    }
}
