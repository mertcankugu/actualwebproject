﻿using SanalGezgin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SanalGezgin.Controllers
{
    public class iletisimController : Controller
    {
        //
        // GET: /iletisim/
        SanalContext context = new SanalContext();

        public ActionResult Index()
        {
            return View();
        }



        [HttpPost]
        public ActionResult mesajYolla(Iletisim mesaj)
        {
            context.Iletisims.Add(mesaj);
            context.SaveChanges();
            return RedirectToAction("/");

        }

    }
}
