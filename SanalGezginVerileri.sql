USE [master]
GO
/****** Object:  Database [SanalGezginVerileri]    Script Date: 18.08.2016 18:11:22 ******/
CREATE DATABASE [SanalGezginVerileri]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SanalGezginVerileri', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\SanalGezginVerileri.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SanalGezginVerileri_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\SanalGezginVerileri_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SanalGezginVerileri] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SanalGezginVerileri].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SanalGezginVerileri] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET ARITHABORT OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SanalGezginVerileri] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SanalGezginVerileri] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SanalGezginVerileri] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SanalGezginVerileri] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET RECOVERY FULL 
GO
ALTER DATABASE [SanalGezginVerileri] SET  MULTI_USER 
GO
ALTER DATABASE [SanalGezginVerileri] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SanalGezginVerileri] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SanalGezginVerileri] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SanalGezginVerileri] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [SanalGezginVerileri] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SanalGezginVerileri', N'ON'
GO
USE [SanalGezginVerileri]
GO
/****** Object:  Table [dbo].[Admin]    Script Date: 18.08.2016 18:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Admin](
	[adminId] [int] IDENTITY(1,1) NOT NULL,
	[adminAdiSoyadi] [nvarchar](50) NULL,
	[adminSifre] [nvarchar](50) NULL,
 CONSTRAINT [PK_Admin] PRIMARY KEY CLUSTERED 
(
	[adminId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Banner]    Script Date: 18.08.2016 18:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Banner](
	[bannerId] [int] IDENTITY(1,1) NOT NULL,
	[bannerBaslik] [nvarchar](150) NULL,
	[bannerIcerik] [nvarchar](250) NULL,
	[bannerYolu] [nvarchar](250) NULL,
	[bannerResim] [nvarchar](250) NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[bannerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Editor]    Script Date: 18.08.2016 18:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Editor](
	[editorId] [int] IDENTITY(1,1) NOT NULL,
	[editorAdiSoyadi] [nvarchar](50) NULL,
	[editorKadi] [nvarchar](50) NULL,
	[editorSifre] [nvarchar](50) NULL,
	[editorTarih] [datetime] NULL CONSTRAINT [DF_Editor_editorTarih]  DEFAULT (getdate()),
	[editorMail] [nvarchar](50) NULL,
	[editorAktif] [bit] NOT NULL CONSTRAINT [DF_Editor_editorAktif]  DEFAULT ((0)),
 CONSTRAINT [PK_Editor] PRIMARY KEY CLUSTERED 
(
	[editorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Iletisim]    Script Date: 18.08.2016 18:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Iletisim](
	[iletisimId] [int] IDENTITY(1,1) NOT NULL,
	[iletisimAdiSoyadi] [nvarchar](50) NULL,
	[iletisimMail] [nvarchar](50) NULL,
	[iletisimKonu] [nvarchar](50) NULL,
	[iletisimIcerik] [nvarchar](max) NULL,
 CONSTRAINT [PK_Iletisim] PRIMARY KEY CLUSTERED 
(
	[iletisimId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Kategori]    Script Date: 18.08.2016 18:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Kategori](
	[kategoriId] [int] IDENTITY(1,1) NOT NULL,
	[kategoriAdi] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Kategori] PRIMARY KEY CLUSTERED 
(
	[kategoriId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Makale]    Script Date: 18.08.2016 18:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Makale](
	[makaleId] [int] IDENTITY(1,1) NOT NULL,
	[makaleBaslik] [nvarchar](150) NOT NULL,
	[makaleIcerik] [nvarchar](max) NOT NULL,
	[makaleTarih] [datetime] NULL CONSTRAINT [DF_Makale_makaleTarih]  DEFAULT (getdate()),
	[kategoriId] [int] NULL,
	[editorId] [int] NULL,
	[videoYolu] [nvarchar](500) NULL,
	[resim1] [nvarchar](500) NULL,
	[resim2] [nvarchar](500) NULL,
	[resim3] [nvarchar](500) NULL,
	[makaleAktif] [bit] NOT NULL CONSTRAINT [DF_Makale_makaleAktif]  DEFAULT ((0)),
	[populer] [bit] NULL CONSTRAINT [DF_Makale_populer]  DEFAULT ((0)),
	[sonHaber] [bit] NULL CONSTRAINT [DF_Makale_sonHaber]  DEFAULT ((0)),
 CONSTRAINT [PK_Makale] PRIMARY KEY CLUSTERED 
(
	[makaleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SosyalAglar]    Script Date: 18.08.2016 18:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SosyalAglar](
	[sosyalId] [int] IDENTITY(1,1) NOT NULL,
	[sosyalAdi] [nvarchar](50) NULL,
	[sosyalHit] [int] NULL,
 CONSTRAINT [PK_SosyalAglar] PRIMARY KEY CLUSTERED 
(
	[sosyalId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Yorum]    Script Date: 18.08.2016 18:11:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Yorum](
	[yorumId] [int] IDENTITY(1,1) NOT NULL,
	[yorumAdiSoyadi] [nvarchar](100) NULL,
	[yorumMail] [nvarchar](100) NULL,
	[yorumIcerik] [nvarchar](1000) NULL,
	[makaleId] [int] NULL,
	[eklenmeTatihi] [datetime] NULL CONSTRAINT [DF_Yorum_eklenmeTatihi]  DEFAULT (getdate()),
	[yorumOnay] [bit] NULL CONSTRAINT [DF_Yorum_yorumOnay]  DEFAULT ((0)),
 CONSTRAINT [PK_Yorum] PRIMARY KEY CLUSTERED 
(
	[yorumId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Admin] ON 

INSERT [dbo].[Admin] ([adminId], [adminAdiSoyadi], [adminSifre]) VALUES (1, N'emre', N'emre')
SET IDENTITY_INSERT [dbo].[Admin] OFF
SET IDENTITY_INSERT [dbo].[Banner] ON 

INSERT [dbo].[Banner] ([bannerId], [bannerBaslik], [bannerIcerik], [bannerYolu], [bannerResim]) VALUES (1, N'Bir Web Sitesinemi İhtiyacınız Var?', N'Web sitesi oluşturmak için kendi zevkinize özel site oluşturabileceğiniz web sitelerine hiç göz attınız mı? ', N'banner', N'/Content/img/banner/big_news_10.jpg')
INSERT [dbo].[Banner] ([bannerId], [bannerBaslik], [bannerIcerik], [bannerYolu], [bannerResim]) VALUES (2, N'Deneme', N'Deneme', N'banner', N'/Content/img/banner/big_news_03.jpg')
SET IDENTITY_INSERT [dbo].[Banner] OFF
SET IDENTITY_INSERT [dbo].[Editor] ON 

INSERT [dbo].[Editor] ([editorId], [editorAdiSoyadi], [editorKadi], [editorSifre], [editorTarih], [editorMail], [editorAktif]) VALUES (1, N'Emre Göktepe', N'emre', N'emre', CAST(N'2016-08-02 00:00:00.000' AS DateTime), N'emregktp@hotmail.com', 1)
INSERT [dbo].[Editor] ([editorId], [editorAdiSoyadi], [editorKadi], [editorSifre], [editorTarih], [editorMail], [editorAktif]) VALUES (2, N'Mertcan Kuğu', N'mertcan', N'mertcan', CAST(N'2016-08-02 00:00:00.000' AS DateTime), N'mrtcnkg@hotmail.com', 1)
INSERT [dbo].[Editor] ([editorId], [editorAdiSoyadi], [editorKadi], [editorSifre], [editorTarih], [editorMail], [editorAktif]) VALUES (9, N'mustafa', N'mustafa', N'mustafa', CAST(N'2016-08-16 14:58:56.937' AS DateTime), N'mustafa@cilingir.com', 0)
INSERT [dbo].[Editor] ([editorId], [editorAdiSoyadi], [editorKadi], [editorSifre], [editorTarih], [editorMail], [editorAktif]) VALUES (10, N'Efe Yıldırım', N'efe', N'efe', CAST(N'2016-08-18 16:11:26.523' AS DateTime), N'efe@yildirim.com', 0)
INSERT [dbo].[Editor] ([editorId], [editorAdiSoyadi], [editorKadi], [editorSifre], [editorTarih], [editorMail], [editorAktif]) VALUES (11, N'Anıl Baran', N'anil', N'anil', CAST(N'2016-08-18 16:11:47.900' AS DateTime), N'anil@baran.com', 0)
SET IDENTITY_INSERT [dbo].[Editor] OFF
SET IDENTITY_INSERT [dbo].[Iletisim] ON 

INSERT [dbo].[Iletisim] ([iletisimId], [iletisimAdiSoyadi], [iletisimMail], [iletisimKonu], [iletisimIcerik]) VALUES (3, N'Musa', N'musa@musa.xom', N'deneme', N'deneme')
SET IDENTITY_INSERT [dbo].[Iletisim] OFF
SET IDENTITY_INSERT [dbo].[Kategori] ON 

INSERT [dbo].[Kategori] ([kategoriId], [kategoriAdi]) VALUES (1, N'İncelemeler')
INSERT [dbo].[Kategori] ([kategoriId], [kategoriAdi]) VALUES (2, N'İnternet')
INSERT [dbo].[Kategori] ([kategoriId], [kategoriAdi]) VALUES (3, N'Oyun')
INSERT [dbo].[Kategori] ([kategoriId], [kategoriAdi]) VALUES (4, N'Mobil')
INSERT [dbo].[Kategori] ([kategoriId], [kategoriAdi]) VALUES (5, N'Programlama')
INSERT [dbo].[Kategori] ([kategoriId], [kategoriAdi]) VALUES (6, N'Sosyal Medya')
SET IDENTITY_INSERT [dbo].[Kategori] OFF
SET IDENTITY_INSERT [dbo].[Makale] ON 

INSERT [dbo].[Makale] ([makaleId], [makaleBaslik], [makaleIcerik], [makaleTarih], [kategoriId], [editorId], [videoYolu], [resim1], [resim2], [resim3], [makaleAktif], [populer], [sonHaber]) VALUES (1, N'Sizin İçin Birbirinden Farklı Kamera Çeşitlerini İnceledik', N'Lorem Ipsum, dizgi ve baskı endüstrisinde kullanılan mıgır metinlerdir. Lorem Ipsum, adı bilinmeyen bir matbaacının bir hurufat numune kitabı oluşturmak üzere bir yazı galerisini alarak karıştırdığı 1500''lerden beri endüstri standardı sahte metinler olarak kullanılmıştır. Beşyüz yıl boyunca varlığını sürdürmekle kalmamış, aynı zamanda pek değişmeden elektronik dizgiye de sıçramıştır. 1960''larda Lorem Ipsum pasajları da içeren Letraset yapraklarının yayınlanması ile ve yakın zamanda Aldus PageMaker gibi Lorem Ipsum sürümleri içeren masaüstü yayıncılık yazılımları ile popüler olmuştur.Neden Kullanırız?Yinelenen bir sayfa içeriğinin okuyucunun dikkatini dağıttığı bilinen bir gerçektir. Lorem Ipsum kullanmanın amacı, sürekli ''buraya metin gelecek, buraya metin gelecek'' yazmaya kıyasla daha dengeli bir harf dağılımı sağlayarak okunurluğu artırmasıdır. Şu anda birçok masaüstü yayıncılık paketi ve web sayfa düzenleyicisi, varsayılan mıgır metinler olarak Lorem Ipsum kullanmaktadır. Ayrıca arama motorlarında ''lorem ipsum'' anahtar sözcükleri ile arama yapıldığında henüz tasarım aşamasında olan çok sayıda site listelenir. Yıllar içinde, bazen kazara, bazen bilinçli olarak (örneğin mizah katılarak), çeşitli sürümleri geliştirilmiştir.Nereden Gelir?Yaygın inancın tersine, Lorem Ipsum rastgele sözcüklerden oluşmaz. Kökleri M.Ö. 45 tarihinden bu yana klasik Latin edebiyatına kadar uzanan 2000 yıllık bir geçmişi vardır. Virginia''daki Hampden-Sydney College''dan Latince profesörü Richard McClintock, bir Lorem Ipsum pasajında geçen ve anlaşılması en güç sözcüklerden biri olan ''consectetur'' sözcüğünün klasik edebiyattaki örneklerini incelediğinde kesin bir kaynağa ulaşmıştır. Lorm Ipsum, Çiçero tarafından M.Ö. 45 tarihinde kaleme alınan "de Finibus Bonorum et Malorum" (İyi ve Kötünün Uç Sınırları) eserinin 1.10.32 ve 1.10.33 sayılı bölümlerinden gelmektedir. Bu kitap, ahlak kuramı üzerine bir tezdir ve Rönesans döneminde çok popüler olmuştur. Lorem Ipsum pasajının ilk satırı olan "Lorem ipsum dolor sit amet" 1.10.32 sayılı bölümdeki bir satırdan gelmektedir.', CAST(N'2016-08-02 00:00:00.000' AS DateTime), 1, 1, NULL, N'incelemeler.jpg', NULL, NULL, 1, 1, 1)
INSERT [dbo].[Makale] ([makaleId], [makaleBaslik], [makaleIcerik], [makaleTarih], [kategoriId], [editorId], [videoYolu], [resim1], [resim2], [resim3], [makaleAktif], [populer], [sonHaber]) VALUES (2, N'İnternetten Bankacılık İşlemleri Yaparken Dikkat Etmeniz Gerekenler', N'Deneme', CAST(N'2016-08-08 20:01:01.767' AS DateTime), 2, 2, NULL, N'internette_tanisip_3_milyonluk_vurgun_yaptilar_h94427_b140b.jpg', NULL, NULL, 1, 1, 1)
INSERT [dbo].[Makale] ([makaleId], [makaleBaslik], [makaleIcerik], [makaleTarih], [kategoriId], [editorId], [videoYolu], [resim1], [resim2], [resim3], [makaleAktif], [populer], [sonHaber]) VALUES (3, N'Need For Speed''i İnceledik Sizler İçin Oyunu Test Ettik', N'Deneme', CAST(N'2016-08-08 20:01:01.767' AS DateTime), 3, 1, NULL, N'Need-for-Speed-No-Limits-Mobil-İnceleme.png', NULL, NULL, 1, 1, NULL)
INSERT [dbo].[Makale] ([makaleId], [makaleBaslik], [makaleIcerik], [makaleTarih], [kategoriId], [editorId], [videoYolu], [resim1], [resim2], [resim3], [makaleAktif], [populer], [sonHaber]) VALUES (4, N'Tavsiye ettiğimiz Uygun Fiyatlı En iyi 10 Android Telefon', N'Deneme', CAST(N'2016-08-08 20:01:28.430' AS DateTime), 4, 1, NULL, N'resized_a13ef-cc891330samsunggalaxyj72016official09.jpg', NULL, NULL, 1, NULL, NULL)
INSERT [dbo].[Makale] ([makaleId], [makaleBaslik], [makaleIcerik], [makaleTarih], [kategoriId], [editorId], [videoYolu], [resim1], [resim2], [resim3], [makaleAktif], [populer], [sonHaber]) VALUES (32, N'Arkaplan Resimlerini Sizler İçin Derledik', N'asd', CAST(N'2016-08-18 04:30:12.867' AS DateTime), 1, 1, NULL, N'29c48d495c3944efad224e2291a9d396.jpg', NULL, NULL, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Makale] OFF
SET IDENTITY_INSERT [dbo].[SosyalAglar] ON 

INSERT [dbo].[SosyalAglar] ([sosyalId], [sosyalAdi], [sosyalHit]) VALUES (1, N'Facebook', 1000)
INSERT [dbo].[SosyalAglar] ([sosyalId], [sosyalAdi], [sosyalHit]) VALUES (2, N'Twitter', 1234)
INSERT [dbo].[SosyalAglar] ([sosyalId], [sosyalAdi], [sosyalHit]) VALUES (3, N'Google', 564)
INSERT [dbo].[SosyalAglar] ([sosyalId], [sosyalAdi], [sosyalHit]) VALUES (4, N'RSS', 94)
INSERT [dbo].[SosyalAglar] ([sosyalId], [sosyalAdi], [sosyalHit]) VALUES (5, N'Pinterest', 847)
INSERT [dbo].[SosyalAglar] ([sosyalId], [sosyalAdi], [sosyalHit]) VALUES (6, N'instagram', 8785)
INSERT [dbo].[SosyalAglar] ([sosyalId], [sosyalAdi], [sosyalHit]) VALUES (7, N'Youtube', 30000)
INSERT [dbo].[SosyalAglar] ([sosyalId], [sosyalAdi], [sosyalHit]) VALUES (8, N'Retri', 2000)
SET IDENTITY_INSERT [dbo].[SosyalAglar] OFF
SET IDENTITY_INSERT [dbo].[Yorum] ON 

INSERT [dbo].[Yorum] ([yorumId], [yorumAdiSoyadi], [yorumMail], [yorumIcerik], [makaleId], [eklenmeTatihi], [yorumOnay]) VALUES (1, N'Deneme', N'deneme@deneme.com', N'Arkadaş Bu Ne biçim site içinde doğru düzgün makale yok netcez böle :D', 1, CAST(N'2016-08-08 00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Yorum] ([yorumId], [yorumAdiSoyadi], [yorumMail], [yorumIcerik], [makaleId], [eklenmeTatihi], [yorumOnay]) VALUES (2, N'Deneme2', N'deneme@denemee.com', N'Deneme2', 1, CAST(N'2016-08-09 00:18:19.810' AS DateTime), 0)
INSERT [dbo].[Yorum] ([yorumId], [yorumAdiSoyadi], [yorumMail], [yorumIcerik], [makaleId], [eklenmeTatihi], [yorumOnay]) VALUES (16, N'Emre Göktepe', N'emregktp@hotmail.com', N'Deneme3', 1, CAST(N'2016-08-12 12:50:20.237' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[Yorum] OFF
ALTER TABLE [dbo].[Makale]  WITH CHECK ADD  CONSTRAINT [FK_Makale_Editor] FOREIGN KEY([editorId])
REFERENCES [dbo].[Editor] ([editorId])
GO
ALTER TABLE [dbo].[Makale] CHECK CONSTRAINT [FK_Makale_Editor]
GO
ALTER TABLE [dbo].[Makale]  WITH CHECK ADD  CONSTRAINT [FK_Makale_Kategori] FOREIGN KEY([kategoriId])
REFERENCES [dbo].[Kategori] ([kategoriId])
GO
ALTER TABLE [dbo].[Makale] CHECK CONSTRAINT [FK_Makale_Kategori]
GO
ALTER TABLE [dbo].[Yorum]  WITH CHECK ADD  CONSTRAINT [FK_Yorum_Makale] FOREIGN KEY([makaleId])
REFERENCES [dbo].[Makale] ([makaleId])
GO
ALTER TABLE [dbo].[Yorum] CHECK CONSTRAINT [FK_Yorum_Makale]
GO
USE [master]
GO
ALTER DATABASE [SanalGezginVerileri] SET  READ_WRITE 
GO
